
const baseUrl = 'https://rest.ehrscape.com/rest/v1';

const username = "ois.seminar";
const password = "ois4fri";

const nutritionAPI_id = "af935d7e";
const nutritionAPI_key = "49ef40eb50530da3517dda3fbfb36e2d";

const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'];

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  let outEhrId = "";
  let ime, priimek, datumRojstva;

  switch (stPacienta) {
      case 1: //star debel povecan BMI in povecan blood pressure
          ime = "Donald";
          priimek = "Trump";
          datumRojstva = "1946-06-14"+"T00:00:00.000Z";
          break;
      case 2: //podhranjen BMI in nenavadno nizka raven kisika
          ime = "Slim";
          priimek = "Jon";
          datumRojstva = "1981-06-19"+"T00:00:00.000Z";
          break;
      case 3: //zdrav
          ime = "Denis";
          priimek = "Zdrav";
          datumRojstva = "1998-06-19"+"T00:00:00.000Z";
          break;
  }

    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (data) {
            let ehrId = data.ehrId;
            let partyData = {
                firstNames: ime,
                lastNames: priimek,
                dateOfBirth: datumRojstva,
                additionalInfo: {"ehrId": ehrId}
            };
            let partyDataStringified = JSON.stringify(partyData);
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                headers: {
                    "Authorization": getAuthorization()
                },
                contentType: 'application/json',
                data: partyDataStringified,
                success: function (party) {
                    if (party.action === 'CREATE') {
                        $("#kreirajSporociloNav").html("<span class='obvestilo label label-success fade-in'>Podatki vpisani</span>");
                        $("#preberiObstojeciEHR").append(new Option(ime+" "+priimek, ehrId));
                        $("#preberiEhrIdZaVitalneZnake").append(new Option(ime+" "+priimek, ehrId));
                        if(stPacienta===1){
                            outEhrId = ehrId;
                            posljiMeritve(ehrId, "191.4", "114.9", "2009-05-20T09:30", "36.1", "95", 135, 90 );
                            posljiMeritve(ehrId, "192.6", "122.0", "2011-06-20T09:30", "36.0", "96",131,85);
                            posljiMeritve(ehrId, "191.5", "119.3", "2013-07-20T09:30", "35.9", "93" ,142,92);
                            posljiMeritve(ehrId, "190.4", "113.2", "2014-08-20T09:30", "35.8", "91" ,133,88);
                            posljiMeritve(ehrId, "190.4", "109.7", "2015-09-20T09:30", "36.2", "92" ,127,82);
                            posljiMeritve(ehrId, "190.3", "108.5", "2016-10-20T09:30", "36.7", "94" ,122, 83);
                        }

                        else if(stPacienta===2){
                            outEhrId = ehrId;
                            posljiMeritve(ehrId, "165.5", "47.2", "2008-01-20T09:30", "37.3", "90" ,115,85);
                            posljiMeritve(ehrId, "165.5", "46.3", "2010-05-20T09:30", "35.5", "89" ,121,89);
                            posljiMeritve(ehrId, "165.4", "45.5", "2012-03-20T09:30", "35.8", "96" ,119,81);
                            posljiMeritve(ehrId, "165.4", "46.2", "2013-06-20T09:30", "36.0", "91" ,118,79);
                            posljiMeritve(ehrId, "165.5", "44.7", "2015-08-20T09:30", "35.7", "92" ,117,83);
                            posljiMeritve(ehrId, "165.4", "48.2", "2016-09-20T09:30", "36.2", "93" ,116,82);
                        }

                        else if(stPacienta===3){
                            outEhrId = ehrId;
                            posljiMeritve(ehrId, "185.4", "73.2", "2014-05-20T09:30", "35.8", "98" ,119,71);
                            posljiMeritve(ehrId, "186.5", "74.5", "2015-02-20T09:30", "35.9", "97" ,120,76);
                            posljiMeritve(ehrId, "187.4", "75.0", "2015-08-20T09:30", "36.5", "99" ,118,77);
                            posljiMeritve(ehrId, "188.3", "77.9", "2016-02-20T09:30", "35.2", "97" ,115,78);
                            posljiMeritve(ehrId, "189.4", "79.5", "2017-04-20T09:30", "36.4", "96" ,114,71);
                            posljiMeritve(ehrId, "190.7", "70.5", "2017-03-20T08:30", "36.1", "99" ,121,80);
                        }
                    }
                },
                error: function(err) {
                    $("#kreirajSporocilo").html("<span class='obvestilo label " +
                        "label-danger fade-in'>Napaka '" +
                        JSON.parse(err.responseText).userMessage + "'!");
                }
            });
        }
    });

  return outEhrId;
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

/**
 * Za dodajanje vitalnih znakov pacienta je pripravljena kompozicija, ki
 * vključuje množico meritev vitalnih znakov (EHR ID, datum in ura,
 * telesna višina, telesna teža, sistolični in diastolični krvni tlak,
 * nasičenost krvi s kisikom in merilec).
 */
function dodajMeritveVitalnihZnakov() {
    let ehrId = $("#vitalniEHRid").val();
    let datumInUra = $("#dodajVitalnoDatumInUra").val();
    let telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
    let telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
    let telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
    let sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
    let diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
    let nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();

    if (!ehrId || ehrId.trim().length == 0) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
        posljiMeritve(ehrId, telesnaVisina, telesnaTeza, datumInUra, telesnaTemperatura, nasicenostKrviSKisikom,sistolicniKrvniTlak,diastolicniKrvniTlak);
    }
}


/**
 * Funkcija za dodajanje meritev vitalnih znakov bolnika, ki je
 * locena od funkcije dodajMeritveVitalnihZnakov zaradi generiranja
 * zacetnih podatkov.
 */

function posljiMeritve(ehrId, telesnaVisina, telesnaTeza, datumInUra, telesnaTemperatura, nasicenostKrviSKisikom, sistolicniKrvniTlak, diastolicniKrvniTlak) {
    let podatki = {
        // Struktura predloge je na voljo na naslednjem spletnem naslovu:
        // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
        "ctx/language": "en",
        "ctx/territory": "SI",
        "ctx/time": datumInUra,
        "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
        "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
        "vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
        "vital_signs/body_temperature/any_event/temperature|unit": "°C",
        "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
        "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
        "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
    };
    let parametriZahteve = {
        ehrId: ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT'
    };
    $.ajax({
        url: baseUrl + "/composition?" + $.param(parametriZahteve),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(podatki),
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (res) {
            $("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-success fade-in'>" +
                res.meta.href + ".</span>");
        },
        error: function(err) {
            $("#dodajMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
        }
    });

}


/**
 * Pridobivanje vseh zgodovinskih podatkov meritev izbranih vitalnih znakov
 * (telesna temperatura in telesna teža).
 */

function preveriMeritveVitalnihZnakov(){
    const ehrId = $("#meritveVitalnihZnakovEHRid").val();

    if(!ehrId || ehrId.trim().length === 0){
        $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
            "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
    } else {
        $("#tempGraf").empty();
        $("#kisikGraf").empty();
        $("#krvniTlakGraf").empty();
        $("#bmiGraf").empty();

        $('#temp').collapse('show');
        generirajBarGraf('body_temperature', ehrId, 'Telesna temperatura [°C]', 'tempGraf', 'temperature');
        $('#blood').collapse('show');
        generirajBarGraf('blood_pressure', ehrId, 'Krvni tlak', 'krvniTlakGraf', '');
        $('#oxygen').collapse('show');
        generirajKisikGraf(ehrId);
        $('#bmi').collapse('show');
        BMIGraf(ehrId);


    }
}

function generirajBarGraf(tip, ehrId, label, elementId, value){
    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/" + tip,
        type: 'GET',
        headers: {
            Authorization: getAuthorization()
        },
        success: function (data) {
            let values = [];
            let labels = [];
            if(tip === 'blood_pressure'){
                let systolic = [];
                let diastolic = [];
                data.forEach(function (el, i, arr) {
                    let date = new Date(el.time);
                    labels[i] = monthNames[date.getMonth()] + '-' + date.getFullYear();
                    systolic[i] = el['systolic'];
                    diastolic[i] = el['diastolic'];
                });
                let element = document.getElementById(elementId).getContext('2d');
                new Chart(element, {
                    type: 'line',
                    data: {
                        labels: labels.reverse(),
                        datasets: [{
                            label: 'Sistolični [mm Hg]',
                            data: systolic.reverse(),
                            yAxisID: 'A',
                            backgroundColor: 'rgba(54, 162, 235, 0.2)',
                            borderColor: 'rgba(54, 162, 235, 1)',
                            borderWidth: 1
                        },{
                            label: 'Diastolični [mm Hg]',
                            yAxisID: 'B',
                            data: diastolic.reverse(),
                            backgroundColor: 'rgba(255, 206, 86, 0.2)',
                            borderColor: 'rgba(255, 206, 86, 1)',
                            borderWidth: 1
                        }],
                    },
                    options: {
                        scales: {
                            yAxes: [
                                { id: 'A', position: 'left', ticks: {beginAtZero: false} },
                                { id: 'B', position: 'right', ticks: {beginAtZero: false} }
                            ]
                        }
                    }
                })

            }else {
                data.forEach(function (el, i, arr) {
                    let date = new Date(el.time);
                    labels[i] = monthNames[date.getMonth()] + '-' + date.getFullYear();
                    values[i] = el[value];
                });
                let element = document.getElementById(elementId).getContext('2d');

                new Chart(element, {
                    type: 'bar',
                    data: {
                        labels: labels.reverse(),
                        datasets: [{
                            label: label,
                            data: values.reverse(),
                            backgroundColor: 'rgba(255, 99, 132, 0.2)',
                            borderColor: 'rgba(255, 99, 132, 1)',
                            borderWidth: 1
                        }]
                    },
                    options: { scales: { yAxes: [{ ticks: {beginAtZero: true} }] } }
                });
            }
        },
        error: function (err) {
            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
        }
    });
}

function generirajKisikGraf(ehrId){
    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/spO2",
        type: 'GET',
        headers: {
            Authorization: getAuthorization()
        },
        success: function (res) {
            var value = res[0].spO2.toFixed(2);
            $('.last-spo2').text(value + "%");
            $('.bar-spo2').css('width', value +"%");
        },
        error: function (err) {
            $("#preberiMeritveVitalnihZnakovSporocilo").html(
                "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                JSON.parse(err.responseText).userMessage + "'!");
        }
    });

}

function BMIGraf(ehrId){
    let weight, height;

    $.ajax({
        url: baseUrl + "/view/" + ehrId + "/weight",
        type: 'GET',
        headers: {
            Authorization: getAuthorization()
        },
        success: function (data) {
            weight = data[data.length-1].weight;
            $.ajax({
                url: baseUrl + "/view/" + ehrId + "/height",
                type: 'GET',
                headers: {
                    Authorization: getAuthorization()
                },
                success: function (res) {
                    height = res[res.length-1].height;
                    console.log(height);
                    console.log(weight);
                    generirajBMIGraf(computeBMI(height, weight));
                }
            });
        }
    });
}

function generirajBMIGraf(bmi) {
    var opts = {
        angle: -0.21, // The span of the gauge arc
        lineWidth: 0.19, // The line thickness
        radiusScale: 1, // Relative radius
        pointer: {
            length: 0.6, // // Relative to gauge radius
            strokeWidth: 0.035, // The thickness
            color: '#000000' // Fill color
        },
        limitMax: false,     // If false, max value increases automatically if value > maxValue
        limitMin: false,     // If true, the min value of the gauge will be fixed
        generateGradient: true,
        highDpiSupport: true,     // High resolution support
        staticZones: [
            {strokeStyle: "#ffdd01", min: 15, max: 18.5},
            {strokeStyle: "#30b32d", min: 18.5, max: 24.99},
            {strokeStyle: "#ff9800", min: 25, max: 29.99},
            {strokeStyle: "#f57c00", min: 30, max: 39.99},
            {strokeStyle: "#d50000", min: 40, max: 50},
        ],
        staticLabels: {
            font: "10px sans-serif",  // Specifies font
            labels: [18.5, 25, 30, 40],  // Print labels at these values
            color: "#000000",  // Optional: Label text color
            fractionDigits: 0  // Optional: Numerical precision. 0=round off.
        },
    };
    let target = document.getElementById('bmiGraf'); // your canvas element
    let gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
    gauge.maxValue = 50; // set max gauge value
    gauge.setMinValue(15);  // Prefer setter over gauge.minValue = 0
    gauge.animationSpeed = 32; // set animation speed (32 is default value)
    gauge.set(bmi[0]); // set actual value
    $('#bmiOpis').html(bmi[1]);
}

function computeBMI(height, weight) {

    let BMI = Math.round(weight / Math.pow(height, 2) * 10000);
    let bmiDet;
    let output = Math.round(BMI * 100) / 100;
    let bmiVal = output + ' kg/m2';
    if (output < 18.5)
        bmiDet = "Underweight";
    else if (output >= 18.5 && output <= 25)
        bmiDet = "Normal";
    else if (output >= 25 && output <= 30)
        bmiDet = "Obese";
    else if (output > 30)
        bmiDet = "Overweight";

    bmiDet = bmiVal + ' ' + bmiDet;
    return [output, bmiDet];
}

function dodajAnalizoHrane(){
    $(".dietResult-label").html('');
    let arr = {
        "ingr": $('#dodajHranoZaAnalizo').val().split(/\n|\r/)
    };
    let totalCal, FAT, totalDailyFAT, FASAT, totalDailyFASAT, FATRN, CHOLE, totalDailyCHOLE, NA, totalDailyNA, CHOCDF, totalDailyCHOCDF, FIBTG, totalDailyFIBTG, SUGAR, SUGARadded, PROCNT, totalDailyPROCNT, VITD, totalDailyVITD, CA, totalDailyCA, FE, totalDailyFE, K, totalDailyK;
    $.ajax({
        url: 'https://api.edamam.com/api/nutrition-details?app_id='+nutritionAPI_id+'&app_key='+nutritionAPI_key,
        type: 'POST',
        data: JSON.stringify(arr),
        contentType: 'application/json',
        success: function(data) {

            if (typeof(data.totalNutrients.ENERC_KCAL) != "undefined") {
                totalCal = Math.round(data.totalNutrients.ENERC_KCAL.quantity);
            } else {totalCal = '0'};

            if (typeof(data.totalNutrients.FAT) != "undefined") {
                FAT = Math.round(data.totalNutrients.FAT.quantity*10)/10+' '+data.totalNutrients.FAT.unit;
            } else {FAT = '-'};
            if (typeof(data.totalDaily.FAT) != "undefined") {
                totalDailyFAT = Math.round(data.totalDaily.FAT.quantity)+' '+data.totalDaily.FAT.unit;
            } else {totalDailyFAT = '-'};

            if (typeof(data.totalNutrients.FASAT) != "undefined") {
                FASAT = Math.round(data.totalNutrients.FASAT.quantity*10)/10+' '+data.totalNutrients.FASAT.unit;
            } else {FASAT = '-'};
            if (typeof(data.totalDaily.FASAT) != "undefined") {
                totalDailyFASAT = Math.round(data.totalDaily.FASAT.quantity)+' '+data.totalDaily.FASAT.unit;
            } else {totalDailyFASAT = '-'};

            if (typeof(data.totalNutrients.FATRN) != "undefined") {
                FATRN = Math.round(data.totalNutrients.FATRN.quantity*10)/10+' '+data.totalNutrients.FATRN.unit;
            } else {FATRN = '-'};

            if (typeof(data.totalNutrients.CHOLE) != "undefined") {
                CHOLE = Math.round(data.totalNutrients.CHOLE.quantity*10)/10+' '+data.totalNutrients.CHOLE.unit;
            } else {CHOLE = '-'};
            if (typeof(data.totalDaily.CHOLE) != "undefined") {
                totalDailyCHOLE = Math.round(data.totalDaily.CHOLE.quantity)+' '+data.totalDaily.CHOLE.unit;
            } else {totalDailyCHOLE = '-'};

            if (typeof(data.totalNutrients.NA) != "undefined") {
                NA = Math.round(data.totalNutrients.NA.quantity*10)/10+' '+data.totalNutrients.NA.unit;
            } else {NA = '-'};
            if (typeof(data.totalDaily.NA) != "undefined") {
                totalDailyNA = Math.round(data.totalDaily.NA.quantity)+' '+data.totalDaily.NA.unit;
            } else {totalDailyNA = '-'};

            if (typeof(data.totalNutrients.CHOCDF) != "undefined") {
                CHOCDF = Math.round(data.totalNutrients.CHOCDF.quantity*10)/10+' '+data.totalNutrients.CHOCDF.unit;
            } else {CHOCDF = '-'};
            if (typeof(data.totalDaily.CHOCDF) != "undefined") {
                totalDailyCHOCDF = Math.round(data.totalDaily.CHOCDF.quantity)+' '+data.totalDaily.CHOCDF.unit;
            } else {totalDailyCHOCDF = '-'};

            if (typeof(data.totalNutrients.FIBTG) != "undefined") {
                FIBTG = Math.round(data.totalNutrients.FIBTG.quantity*10)/10+' '+data.totalNutrients.FIBTG.unit;
            } else {FIBTG = '-'};
            if (typeof(data.totalDaily.FIBTG) != "undefined") {
                totalDailyFIBTG = Math.round(data.totalDaily.FIBTG.quantity)+' '+data.totalDaily.FIBTG.unit;
            } else {totalDailyFIBTG = '-'};

            if (typeof(data.totalNutrients.SUGAR) != "undefined") {
                SUGAR = Math.round(data.totalNutrients.SUGAR.quantity*10)/10+' '+data.totalNutrients.SUGAR.unit;
            } else {SUGAR = '-'};

            if (typeof(data.totalNutrients.SUGARadded) != "undefined") {
                SUGARadded = Math.round(data.totalNutrients.SUGARadded.quantity*10)/10+' '+data.totalNutrients.SUGARadded.unit;
            } else {SUGARadded = '-'};

            if (typeof(data.totalNutrients.PROCNT) != "undefined") {
                PROCNT = Math.round(data.totalNutrients.PROCNT.quantity*10)/10+' '+data.totalNutrients.PROCNT.unit;
            } else {PROCNT = '-'};
            if (typeof(data.totalDaily.PROCNT) != "undefined") {
                totalDailyPROCNT = Math.round(data.totalDaily.PROCNT.quantity)+' '+data.totalDaily.PROCNT.unit;
            } else {totalDailyPROCNT = '-'};

            if (typeof(data.totalNutrients.VITD) != "undefined") {
                VITD = Math.round(data.totalNutrients.VITD.quantity*10)/10+' '+data.totalNutrients.VITD.unit;
            } else {VITD = '-'};
            if (typeof(data.totalDaily.VITD) != "undefined") {
                totalDailyVITD = Math.round(data.totalDaily.VITD.quantity)+' '+data.totalDaily.VITD.unit;
            } else {totalDailyVITD = '-'};

            if (typeof(data.totalNutrients.CA) != "undefined") {
                CA = Math.round(data.totalNutrients.CA.quantity*10)/10+' '+data.totalNutrients.CA.unit;
            } else {CA = '-'};
            if (typeof(data.totalDaily.CA) != "undefined") {
                totalDailyCA = Math.round(data.totalDaily.CA.quantity)+' '+data.totalDaily.CA.unit;
            } else {totalDailyCA = '-'};

            if (typeof(data.totalNutrients.FE) != "undefined") {
                FE = Math.round(data.totalNutrients.FE.quantity*10)/10+' '+data.totalNutrients.FE.unit;
            } else {FE = '-'};
            if (typeof(data.totalDaily.FE) != "undefined") {
                totalDailyFE = Math.round(data.totalDaily.FE.quantity)+' '+data.totalDaily.FE.unit;
            } else {totalDailyFE = '-'};

            if (typeof(data.totalNutrients.K) != "undefined") {
                K = Math.round(data.totalNutrients.K.quantity*10)/10+' '+data.totalNutrients.K.unit;
            } else {K = '-'};
            if (typeof(data.totalDaily.K) != "undefined") {
                totalDailyK = Math.round(data.totalDaily.K.quantity)+' '+data.totalDaily.K.unit;
            } else {totalDailyK = '-'};

            var $msg = $('<div class="col"></div>');
            $msg.append('<section class="performance-facts" id="performance-facts">'+
                '	<div class="performance-facts__header">'+
                '		<h1 class="performance-facts__title">Prehranske vrednosti</h1>'+
                '		<p><span id="lnumser">0</span> servings per container</p>'+
                '	</div>'+
                '	<table class="performance-facts__table">'+
                '		<thead>'+
                '			<tr>'+
                '				<th colspan="3" class="amps">Količina na porcijo</th>'+
                '			</tr>'+
                '		</thead>'+
                '		<tbody>'+
                '			<tr>'+
                '				<th colspan="2" id="lkcal-val-cal"><b>Kalorije</b></th>'+
                '				<td class="nob">'+totalCal+'</td>'+
                '			</tr>'+
                '			<tr class="thick-row">'+
                '				<td colspan="3" class="small-info"><b>% Dnevne vrednosti*</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<th colspan="2"><b>Skupaj maščobe</b> '+FAT+'</th>'+
                '				<td><b>'+totalDailyFAT+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<td class="blank-cell"></td>'+
                '				<th>Nasičene maščobe '+FASAT+'</th>'+
                '				<td><b>'+totalDailyFASAT+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<td class="blank-cell"></td>'+
                '				<th>Transmaščobe '+FATRN+'</th>'+
                '				<td></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<th colspan="2"><b>Holesterol</b> '+CHOLE+'</th>'+
                '				<td><b>'+totalDailyCHOLE+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<th colspan="2"><b>Natrij</b> '+NA+'</th>'+
                '				<td><b>'+totalDailyNA+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<th colspan="2"><b>Skupaj ogljikovihidrati</b> '+CHOCDF+'</th>'+
                '				<td><b>'+totalDailyCHOCDF+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<td class="blank-cell"></td>'+
                '				<th>Prehranske vlaknine '+FIBTG+'</th>'+
                '				<td><b>'+totalDailyFIBTG+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<td class="blank-cell"></td>'+
                '				<th>Skupno sladkorji '+SUGAR+'</th>'+
                '				<td></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<td class="blank-cell"></td>'+
                '				<th>Vsebuje '+SUGARadded+' dodanih sladkorjev</th>'+
                '				<td></td>'+
                '			</tr>'+
                '			<tr class="thick-end">'+
                '				<th colspan="2"><b>Proteini</b> '+PROCNT+'</th>'+
                '				<td><b>'+totalDailyPROCNT+'</b></td>'+
                '			</tr>'+
                '		</tbody>'+
                '	</table>'+
                '	<table class="performance-facts__table--grid">'+
                '		<tbody>'+
                '			<tr>'+
                '				<th>Vitamin D '+VITD+'</th>'+
                '				<td><b>'+totalDailyVITD+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<th>Kalcij '+CA+'</th>'+
                '				<td><b>'+totalDailyCA+'</b></td>'+
                '			</tr>'+
                '			<tr>'+
                '				<th>Železo '+FE+'</th>'+
                '				<td><b>'+totalDailyFE+'</b></td>'+
                '			</tr>'+
                '			<tr class="thin-end">'+
                '				<th>Kalij '+K+'</th>'+
                '				<td><b>'+totalDailyK+'</b></td>'+
                '			</tr>'+
                '		</tbody>'+
                '	</table>'+
                '	<p class="small-info" id="small-nutrition-info">*Procent dnevne vrednosti je osnovan na 2000 kalorijski dieti</p>'+
                '</section>');
            $(".dietResult-label").append($msg);

        }
    });
}


$(document).ready(function() {



    /**
     * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
     * ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
     */
    $('#preberiObstojeciEHR').change(function() {
        $("#preberiSporocilo").html("");
        $("#vitalniEHRid").val($(this).val());
        $("#dodajVitalneIme").html($('#preberiObstojeciEHR option:selected').text());
    });

    /**
     * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
     * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
     * (npr. Ata Smrk, Pujsa Pepa)
     */
    $('#preberiEhrIdZaVitalneZnake').change(function() {
        $("#preberiMeritveVitalnihZnakovSporocilo").html("");
        $("#rezultatMeritveVitalnihZnakov").html("");
        $("#meritveVitalnihZnakovEHRid").val($(this).val());
        $("#dodajImeVnos").html($('#preberiEhrIdZaVitalneZnake option:selected').text());
    });

});
