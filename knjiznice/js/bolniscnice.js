/**
 * Od tu naprej koda za implementacijo seznama bolniscnic.
 */

/* global L, distance */

var pot;

// seznam z markerji na mapi
var markerji = [];

var mapa;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */


window.addEventListener('load', function () {

    // Osnovne lastnosti mape
    let mapOptions = {
        center: [46.048879, 14.508584],
        zoom: 12
        // maxZoom: 3
    };

    // Ustvarimo objekt mapa
    mapa = new L.map('mapa_id', mapOptions);

    // Ustvarimo prikazni sloj mape
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

    // Prikazni sloj dodamo na mapo
    mapa.addLayer(layer);

    pridobiPodatke(function(jsonRezultat){
        fixPodatki(jsonRezultat, function (out) {
            izrisRezultatov(out);
        })
    });


    // Objekt oblačka markerja
    var popup = L.popup();

    function obKlikuNaMapo(e) {
        var latlng = e.latlng;
        popup
            .setLatLng(latlng)
            .setContent("Izbrana točka:" + latlng.toString())
            .openOn(mapa);

        posodobiOznakeNaZemljevidu(e.latlng);
    }

    mapa.on('click', obKlikuNaMapo);

    let radij = 5;

    let lokacija;

    mapa.on("click", function(e){
        lokacija = e;
        obKlikuNaMapo(e);
    });

});

/**
 * Za podano vrsto interesne točke dostopaj do JSON datoteke
 * in vsebino JSON datoteke vrni v povratnem klicu
 *
 * @param vrstaInteresneTocke "fakultete" ali "restavracije"
 * @param callback povratni klic z vsebino zahtevane JSON datoteke
 */
function pridobiPodatke(callback) {

    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
    xobj.onreadystatechange = function () {
        // rezultat ob uspešno prebrani datoteki
        if (xobj.readyState == 4 && xobj.status == "200") {
            var json = JSON.parse(xobj.responseText);

            // nastavimo ustrezna polja (število najdenih zadetkov)

            // vrnemo rezultat
            callback(json);
        }
    };
    xobj.send(null);
}


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah,
 * z dodatnim opisom, ki se prikaže v oblačku ob kliku in barvo
 * ikone, glede na tip oznake (FRI = rdeča, druge fakultete = modra in
 * restavracije = zelena)
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 * @param tip "FRI", "restaurant" ali "faculty"
 */
function dodajMarker(lat, lng, ime, naslov) {
    let tip = 'FRI';
    let ikona = new L.Icon({
        iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
            'marker-icon-2x-' +
            (tip == 'FRI' ? 'red' : (tip == 'restaurant' ? 'green' : 'blue')) +
            '.png',
        shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
            'marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });

    // Ustvarimo marker z vhodnima podatkoma koordinat
    // in barvo ikone, glede na tip
    var marker = L.marker([lat, lng], {icon: ikona});

    // Izpišemo želeno sporočilo v oblaček
    if(ime !== undefined && naslov !== undefined) marker.bindPopup("<div>"+ime+"<br>"+naslov+"</div");
    else marker.bindPopup("<div>Ni podatkov</div>")

    // Dodamo točko na mapo in v seznam
    marker.addTo(mapa);
    markerji.push(marker);
}

function dodajPoligon(points, ime, naslov){

    let poligon = L.polygon(points);

    poligon.addTo(mapa);
    if(ime !== undefined && naslov !== undefined) poligon.bindPopup("<div>"+ime+"<br>"+naslov+"</div");
    else poligon.bindPopup("<div>Ni podatkov</div>")

    markerji.push(poligon);

    //if(poligon !== 'undefined') poligon.addTo(mapa);
}

function fixPodatki(podatki, callback){

    let znacilnosti = podatki.features;

    for(let i = 0; i < znacilnosti.length; i++){
        let bolnica = znacilnosti[i].geometry.coordinates;

        if(znacilnosti[i].geometry.type === "LineString"){
            for(let j=0; j < bolnica.length; j++){
                let tmp = bolnica[j][0];
                bolnica[j][0] = bolnica[j][1];
                bolnica[j][1] = tmp;
            }

        }else if(znacilnosti[i].geometry.type === "Polygon"){
            for(let j=0; j < bolnica.length; j++){
                for(let k=0; k < bolnica[j].length; k++){
                    let tmp = bolnica[j][k][0];
                    bolnica[j][k][0] = bolnica[j][k][1];
                    bolnica[j][k][1] = tmp;
                }
            }

        }
    }

    callback(podatki);
}


/**
 * Na podlagi podanih interesnih točk v GeoJSON obliki izriši
 * posamezne točke na zemljevid
 *
 * @param jsonRezultat interesne točke v GeoJSON obliki
 */
function izrisRezultatov(jsonRezultat) {
    let znacilnosti = jsonRezultat.features;

    for (var i = 0; i < znacilnosti.length; i++) {
        // pridobimo koordinate
        let props = znacilnosti[i].properties;
        let ime, naslov;
        if (props["name"] !== undefined) ime = props["name"];
        if(props["addr:housenumber"] !== undefined) naslov = props["addr:street"]+props["addr:housenumber"]+", "+props["addr:city"];

        if(znacilnosti[i].geometry.type === "Point"){

            let lng = znacilnosti[i].geometry.coordinates[0];
            let lat = znacilnosti[i].geometry.coordinates[1];
            dodajMarker(lat, lng, ime, naslov);

        }else if (props["type"] === 'multipolygon'){

            let points1 = znacilnosti[i].geometry.coordinates[0];
            let points2 = znacilnosti[i].geometry.coordinates[1];
            dodajPoligon(points1, name, naslov);
            dodajPoligon(points2, name, naslov);

        } else if (znacilnosti[i].geometry.type !== "LineString"){

            let points = znacilnosti[i].geometry.coordinates;
            dodajPoligon(points, ime, naslov);

        }else{
            let points = znacilnosti[i].geometry.coordinates;
            dodajPoligon(points, ime, naslov);
        }
    }
}


/**
 * Preveri ali izbrano oznako na podanih GPS koordinatah izrišemo
 * na zemljevid glede uporabniško določeno vrednost radij, ki
 * predstavlja razdaljo od FRI.
 *
 * Če radij ni določen, je enak 0 oz. je večji od razdalje izbrane
 * oznake od FRI, potem oznako izrišemo, sicer ne.
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param location je trenutna lokacija
 */

function posodobiOznakeNaZemljevidu(location) {
    for (let i=1; i < markerji.length; i++) {
        if(markerji[i]._latlngs !== undefined ) {
            let lng = markerji[i]._latlngs[0][0].lng;
            let lat = markerji[i]._latlngs[0][0].lat;
            markerji[i].setStyle({
                color: prikaziOznako(lng, lat, location) ? 'green' : 'blue'
            });
        }
    }
}

function prikaziOznako(lng, lat, location) {
    const radij = 15;
    //console.log(lat+" "+lng+" "+location.lat+" "+location.lng+" "+distance(lat, lng, location.lat, location.lng, "K"));
    return distance(lat, lng, location.lat, location.lng, "K") < radij;
}